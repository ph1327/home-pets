<?php
require_once 'conexao/conectaBanco.php';
require_once 'layout/layout.php';

// Aqui você se conecta ao banco
$mysql = new Conexao;

$db = $mysql->conectarBanco();

$id = $_GET['id'];

// Executa uma consulta que pega cinco notícias
$sql = "SELECT * FROM `passeio` WHERE id = '$id'";
$query = $db->query($sql);
$result = $query->fetch_array();
?>
<!DOCTYPE html>
<html lang="pt_BR">
<head>
    <title>Passeio Funcionários</title>
</head>
<body>   
    <div class="container"><br>
      <form action="salvarEditFuncionario.php" method="post">
        <div class="row">
          <div class="col-md-4">
            <label>Nome</label>
            <input type="text" name="nome" class="form-control" value="<?php echo $result['nome'];?>" readOnly>
          </div>
          <div class="col-md-4">
            <label>Data</label>
            <input type="text" name="data" class="form-control" value="<?php echo  date_format(new DateTime($result['data']), 'd/m/Y H:i:s');?>" readOnly>
          </div>
          <div class="col-md-4">
            <label>Status</label>
            <select name="status" class="form-control">
              <option value="Aguardando" <?php echo $result['status'] == 'Aguardando' ? 'Selected' : ''; ?>>Aguardando</option>
              <option value="Confirmado" <?php echo $result['status'] == 'Confirmado' ? 'Selected' : ''; ?>>Confirmado</option>
              <option value="Cancelado" <?php echo $result['status'] == 'Cancelado' ? 'Selected' : ''; ?>>Cancelado</option>
            </select>
          </div>
          <input type="hidden" name="id" value="<?php echo $result['id'];?>">
        </div><br>
        <div class="row">
          <div class="col-md-10"></div>
          <div class="col-md-2">
            <a href="passeiosFuncionarios.php" class="btn btn-danger">Voltar</a>
            <input type="submit" class="btn btn-success" style="cursor: pointer;">
          </div>
        </div>
      </div>
    </form>
</body>
</html>