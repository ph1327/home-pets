<?php
require_once 'Core/UtilFunctions.php';
require_once 'conexao/conectaBanco.php';

$userId = $_GET['uid'];
$hotelId = $_GET['hid'];
$checkIn = new DateTime($_POST['checkin']);
$checkIn = $checkIn->format("Y-m-d H:i:s");
$checkOut = new DateTime($_POST['checkout']);
$checkOut = $checkOut->format("Y-m-d H:i:s");

$mysql = new Conexao;
$db = $mysql->conectarBanco();

if ($db->query("INSERT INTO `reserves`(`checkin_date`, `checkout_date`, `user_id`, `hotel_id`)
                VALUES ('$checkIn', '$checkOut', $userId, $hotelId)"))
    UtilFunctions::redirect('hotelaria.php?success=true&msg=Reserva+criada+com+sucesso');
else
    UtilFunctions::redirect('hotelaria.php?error=true&msg=Falha+ao+criar+reserva');

