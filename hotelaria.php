<?php

require_once 'layout/layout.php';

require_once 'conexao/conectaBanco.php';

$mysql = new Conexao;
$db = $mysql->conectarBanco();

$sql = "SELECT * FROM `hotel`";
$query = $db->query($sql);

$hotels = array();

while ($data = $query->fetch_assoc()) {
    $hotels[] = [
        'id' => $data['id'],
        'name' => $data['name'],
        'address' => $data['address']
    ];
}

?>
<!DOCTYPE html>
<html lang="pt_BR">
<head>
    <title>Hotelaria</title>
</head>
<body>
<div class="container py-5">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header" style="background: #fbfbfb">
                    <strong style="color: black;">Hotelaria</strong>
                </div>
            </div>
        </div>
        <?php
        if (isset($_GET['success'])) {
            if ($_GET['success']) {
                $msg = $_GET['msg'];
                echo("<div style='width: 100%; margin-top: 20px;' class='alert-box cadastro_success'>$msg</div>");
            }
        }
        ?>
        <?php
        if (isset($_GET['error'])) {
            if ($_GET['error']) {
                $msg = $_GET['msg'];
                echo("<div style='width: 100%; margin-top: 20px;' class='alert alert-danger'>$msg</div>");
            }
        }
        ?>
        <table id="example" class="table table-striped table-bordered" style="width:100%; margin-top: 20px;">
            <thead>
            <tr style="white-space: nowrap;">
                <th>Nome</th>
                <th>Endereço</th>
                <th>Agendar</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Nome</th>
                <th>Endereço</th>
                <th>Agendar</th>
            </tr>
            </tfoot>
            <tbody>
            <?php
            foreach ($hotels as $hotel) {
                $id = $hotel['id'];
                echo "<tr style='white-space: nowrap;'>
                            <td style='width: 100px'>" . $hotel['name'] . "</td>" .
                    "<td style='width: 100px'>" . $hotel['address'] . "</td>" .
                    "<td style='width: 100px'><a style='color: green;' href='reserve.php?id=$id'>Reservar</a></td>
                     </tr>";
            }
            ?>
            </tbody>
        </table>
    </div>
</div>


</body>
</html>