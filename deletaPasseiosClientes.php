<?php
require_once 'Core/UtilFunctions.php';
require_once 'conexao/conectaBanco.php';

$id = $_GET['id'];

$mysql = new Conexao;
$db = $mysql->conectarBanco();

if ($db->query("DELETE FROM `passeio` WHERE `id` = '$id'"))
    UtilFunctions::redirect('passeiosClientes.php?success=true&msg=Passeio+deletado+com+sucesso');
else
    UtilFunctions::redirect('passeiosClientes.php?error=true&msg=Falha+ao+deletar+passeio');