<?php
require_once 'conexao/conectaBanco.php';
require_once 'Core/UtilFunctions.php';

if (!isset($_POST['nome'], $_POST['email'], $_POST['telefone'], $_POST['endereco'], $_POST['password'], $_POST['tipo']))
    UtilFunctions::redirect("cadastro.php?error=true&msg=Invalid+credentials");

$mysql = new Conexao;

$db = $mysql->conectarBanco();

$nome = $_POST['nome'];
$email = $db->escape_string($_POST['email']);
$endereco = $_POST['endereco'];
$password =  password_hash(($_POST['password']), PASSWORD_DEFAULT);
$telefone =  $_POST['telefone'];
$tipo = $_POST['tipo'];

$sql = "INSERT INTO `user` (`nome`, `email`, `fk_user_type`, `senha`, `telefone`, `endereco`) VALUES ('$nome', '$email', '$tipo', '$password', '$telefone', '$endereco');";
$query = $db->query($sql);

if (! $query)
	UtilFunctions::redirect("cadastro.php?error=true&msg=Invalid+credentials");

UtilFunctions::redirect("index.php?success=true&msg=Cadastrado+com+sucesso");

// cadastro is success