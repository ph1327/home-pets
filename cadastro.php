<?php 

require_once 'layout/layout.php';
require_once 'conexao/conectaBanco.php';

$mysql = new Conexao;

$conexao = $mysql->conectarBanco();

$sql = "SELECT * FROM user_type";
$query = $conexao->query($sql);
$result = $query->fetch_all(MYSQLI_ASSOC);
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cadastro</title>
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="container-conteudo-cadastro">

				<form class="login100-form validate-form" method="post" action="cadastrarBanco.php">
					<span class="login100-form-title">
						<i class="fa fa-paw"></i> Cadastro <i class="fa fa-paw"></i>
					</span>

					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="nome" placeholder="Nome Completo" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="email" name="email" placeholder="Email" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="telefone" placeholder="Telefone" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-phone" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="endereco" placeholder="Endereço" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-address-card" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Senha" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required" style="margin-left: 30%;"><br>
						<?php foreach ($result as $key):?>
							<input type="radio" name="tipo" value="<?php echo $key['id']  ?>" required><span style="margin-left: 10px;"><?php echo $key['nome']; ?></span><br>
						<?php endforeach ?>
					</div>
					 <?php
		                if (isset($_GET['error'])) {
		                    if ($_GET['error']) {
		                        $msg = $_GET['msg'];
		                        echo("<p style='text-align: center; padding-top: 20px; color: red;'>$msg</p>");
		                    }
		                }
		              ?>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Cadastrar
						</button>
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="index.php">
							Voltar
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>