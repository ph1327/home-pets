<?php

require_once 'layout/layout.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home - Cliente</title>
    <style type="text/css">
        .container-imagem {
            position: relative;
            height: 100%;
            width: 100%;
            /* adicionando imagem de fundo */
            background: url('images/fundo.jpg');
            background-size: cover;
            opacity: 0.9;
        }

        .texto {
              position: absolute;
              background-color: rgba(0,0,0,0.5);
              color:#fff;
              text-align: center;
              top: 35%;
              width: 50%;
              line-height: 300px;
              height: 300px;
              font-size: 80px;
        }
        
    </style>
</head>
<body style="margin: 0;overflow:hidden;">   
    <div class="container-imagem"></div>
    <div class="container">
        <div class="texto"><i class="fa fa-paw"></i>  Bem vindo <?php echo $_SESSION['user']['nome'] ?> <i class="fa fa-paw"></i></div>    
    </div>
    
</body>
</html>