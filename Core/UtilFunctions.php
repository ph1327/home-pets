<?php


class UtilFunctions
{
    public static function redirect(string $location)
    {
        header("Location: " . $location);
        exit;
    }
}