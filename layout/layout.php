<?php
require_once 'Core/GlobalEnum.php';
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>
<body>
<?php
if ($_SESSION != null) {
    ?>
    <div class="container"><br>
        <nav class="navbar navbar-expand-lg ">
	        <span class="principal100-form-title">
                <?php
                if ($_SESSION['user']['fk_user_type'] == GlobalEnum::CLIENT_TYPE_ID)
	               echo "<a class='nav-link' href='principalCliente.php' style='font-size: 20px'>Home Pets <i class='fa fa-paw'></i></a>";                    
               else
                   echo "<a class='nav-link' href='principalFuncionario.php' style='font-size: 20px'>Home Pets <i class='fa fa-paw'></i></a>";                     
                ?>

	        </span>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <?php 
                        foreach ($_SESSION as $key) {
                            if ($key['fk_user_type'] == 1) { ?>
                                <a class="nav-link" href="passeiosClientes.php" style="font-size: 20px">Passeios</a>    
                            <?php }else{ ?>
                                <a class="nav-link" href="passeiosFuncionarios.php" style="font-size: 20px">Passeios</a>    
                            <?php }
                        }?>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="hotelaria.php" style="font-size: 20px">Hotelaria</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="font-size: 20px">Ajuda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="font-size: 20px">Mais sobre nós</a>
                    </li>
                    <?php
                    if ($_SESSION['user']['fk_user_type'] == GlobalEnum::CLIENT_TYPE_ID)
                        echo "<a class='nav-link' href='minhas-reservas.php' style='font-size: 20px'>Minhas reservas</a>";
                    ?>
                    <li class="nav-item">
                        <a class="nav-link btn btn-danger" href="logout.php" style="font-size: 20px">Logout</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
<?php } ?>

</body>
</html>