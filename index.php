<?php

require_once 'layout/layout.php';

?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <title>Login</title>
</head>
<body>
<div class="limiter">

    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="images/img-01.png" alt="IMG">
            </div>
            
           <form class="login100-form validate-form" method="POST" action="login.php">
                <?php  
                if (isset($_GET['success'])) {
                    if ($_GET['success']) {
                        $msg = $_GET['msg'];
                        echo("<div class='alert-box cadastro_success'>$msg</div>");
                    }
                }
            ?>
					<span class="login100-form-title">
						Home Pets <i class="fa fa-paw"></i>
					</span>

                <div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                    <input class="input100" type="text" name="email" placeholder="Email" required>
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <input class="input100" type="password" name="pass" placeholder="Senha" required>
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Entrar
                    </button>
                </div>
                <?php
                    if (isset($_GET['error'])) {
                        if ($_GET['error']) {
                            $msg = $_GET['msg'];
                            echo("<p style='text-align: center; padding-top: 20px; color: red;'>$msg</p>");
                        }
                    }                  
                ?>
                <div class="text-center p-t-12">
						<span class="txt1">
							Esqueceu
						</span>
                    <a class="txt2" href="#">
                        Usuário / Senha?
                    </a>
                </div>

                <div class="text-center p-t-136">
                    <a class="txt2" href="cadastro.php">
                        Criar sua conta
                        <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>


<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/tilt/tilt.jquery.min.js"></script>
<script>
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="js/main.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){  // A DIFERENÇA ESTA AQUI, EXECUTA QUANDO O DOCUMENTO ESTA "PRONTO"
       $( "div.cadastro_success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
    });
</script>

</body>
</html>