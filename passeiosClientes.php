<?php
require_once 'conexao/conectaBanco.php';
require_once 'layout/layout.php';


// Aqui você se conecta ao banco
$mysql = new Conexao;
$db = $mysql->conectarBanco();

$userId = $_SESSION['user']['id'];
$sql = "SELECT p.* FROM `passeio` p
         JOIN user u on p.cliente_id = u.id
        WHERE `cliente_id` = $userId";
$query = $db->query($sql);

?>

<!DOCTYPE html>
<html lang="pt_BR">
<head>
    <title>Passeio</title>
</head>
<body>   
   <div class="container">
        <div class="col-12">
            <div class="card">
                <div class="card-header" style="background: #fbfbfb">
                    <strong style="color: black;">Passeios - <?php echo $_SESSION['user']['nome'] ?> </strong>
                </div>
            </div>
        </div>
        <?php
        if (isset($_GET['success'])) {
            if ($_GET['success']) {
                $msg = $_GET['msg'];
                echo("<div style='width: 100%; margin-top: 20px;' class='alert-box cadastro_success'>$msg</div>");
            }
        }
        ?>
        <?php
        if (isset($_GET['error'])) {
            if ($_GET['error']) {
                $msg = $_GET['msg'];
                echo("<div style='width: 100%; margin-top: 20px;' class='alert alert-danger'>$msg</div>");
            }
        }
        ?>
        <br>
        <div class="row">
            <div class="col-md-10">
                
            </div>
            <div class="col-md-2">
                <a href="insertPasseiosClientes.php" class="btn btn-success">Adicionar</a>
            </div>
        </div>
        <br>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col" style="text-align: center;">Nome</th>
              <th scope="col" style="text-align: center;">Data</th>
              <th scope="col" style="text-align: center;">Status</th>
              <th scope="col" style="text-align: center;">Ações</th>
            </tr>
          </thead>
          <?php while ($dados = $query->fetch_array()) {?> 
              <tbody>
                <tr>
                  <td style="text-align: center;"><?php echo $dados['nome']; ?></td>
                  <td style="text-align: center;"><?php echo date_format(new DateTime($dados['data']), 'd/m/Y H:i:s'); ?></td>
                  <?php if($dados['status'] == 'Confirmado'){ ?>
                    <td style="text-align: center;background-color: #28a745; color: white;">
                      <i class="fa fa-check"></i>
                      <?php echo $dados['status']; ?>
                    </td>  
                  <?php } else if($dados['status'] == 'Cancelado'){ ?>
                    <td style="text-align: center;background-color: #dc3545; color: white;">
                      <i class="fa fa-times"></i>
                      <?php echo $dados['status']; ?>
                    </td>
                  <?php } else{ ?>
                    <td style="text-align: center;background-color: #ffc107; color: white;">
                      <i class="fa fa-hourglass-start"></i>
                      <?php echo $dados['status']; ?>
                    </td>
                  <?php } ?>
                  <td style="text-align: center;">
                    <?php if($dados['status'] != 'Confirmado'){ ?>
                      <a href="deletaPasseiosClientes.php?id=<?php echo $dados['id']?>" class="btn btn-danger" onclick="return confirm('Você tem certeza que deseja deletar esse registro?')">Excluir </a>
                    <?php } ?> 
                  </td>
                </tr>
              </tbody>
          <?php } ?>
        </table>
    </div>

</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){  // A DIFERENÇA ESTA AQUI, EXECUTA QUANDO O DOCUMENTO ESTA "PRONTO"
       $( "div.cadastro_success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
       $( "div.alert-danger" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
    });
</script>