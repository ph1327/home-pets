<?php

require_once 'layout/layout.php';
?>
<!DOCTYPE html>
<html lang="pt_BR">
<head>
    <title>Passeio Funcionários</title>
</head>
<body>   
    <div class="container"><br>
      <form action="salvarInsertCliente.php" method="post">
        <div class="row">
          <div class="col-md-4">
            <label>Data</label>
            <input type="datetime-local" name="data" class="form-control">
          </div>
          <div class="col-md-4">
            <a href="passeiosClientes.php" class="btn btn-danger">Voltar</a>
            <input type="submit" class="btn btn-success" style="cursor: pointer;">
          </div>
        </div><br>
        <input type="hidden" name="id" value="<?php echo $_SESSION['user']['id'];?>">
        <input type="hidden" name="nome" value="<?php echo $_SESSION['user']['nome'];?>">   
      </div>

    </form>
</body>
</html>