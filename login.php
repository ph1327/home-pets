<?php
require_once 'conexao/conectaBanco.php';
require_once 'Core/UtilFunctions.php';
session_start();

if (!isset($_POST['email'], $_POST['pass']))
    UtilFunctions::redirect("index.php?error=true&msg=Invalid+credentials");

$mysql = new Conexao;

$db = $mysql->conectarBanco();

$email = $db->escape_string($_POST['email']);
$password = $_POST['pass'];

$sql = "SELECT * FROM `user` WHERE `email` = '$email'";
$query = $db->query($sql);
$result = $query->fetch_assoc();

if (empty($result))
    UtilFunctions::redirect("index.php?error=true&msg=Invalid+credentials");

if (!password_verify($password, $result['senha']))
    UtilFunctions::redirect("index.php?error=true&msg=Invalid+credentials");

$_SESSION['user'] = $result;

if ($result['fk_user_type'] == 1)
    UtilFunctions::redirect("principalCliente.php");
else
    UtilFunctions::redirect("principalFuncionario.php");
// tela do funcionario