<?php

require_once 'layout/layout.php';

require_once 'conexao/conectaBanco.php';
$mysql = new Conexao;
$db = $mysql->conectarBanco();

$userId = $_SESSION['user']['id'];
$sql = "SELECT * FROM `reserves` r
         JOIN hotel h on r.hotel_id = h.id
        WHERE `user_id` = $userId";
$query = $db->query($sql);

$reserves = array();

while ($data = $query->fetch_assoc()) {
    $reserves[] = [
        'checkin_date' => $data['checkin_date'],
        'checkout_date' => $data['checkout_date'],
        'hotel' => $data['name'],
        'address' => $data['address']
    ];
}
?>
<!DOCTYPE html>
<html lang="pt_BR">
<head>
    <title>Minhas reservas</title>
</head>
<body>
<div class="container py-5">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header" style="background: #fbfbfb">
                    <strong style="color: black;">Minhas reservas</strong>
                </div>
            </div>
        </div>
        <?php
        if (isset($_GET['success'])) {
            if ($_GET['success']) {
                $msg = $_GET['msg'];
                echo("<div style='width: 100%; margin-top: 20px;' class='alert-box cadastro_success'>$msg</div>");
            }
        }
        ?>
        <?php
        if (isset($_GET['error'])) {
            if ($_GET['error']) {
                $msg = $_GET['msg'];
                echo("<div style='width: 100%; margin-top: 20px;' class='alert alert-danger'>$msg</div>");
            }
        }
        ?>
        <table id="example" class="table table-striped table-bordered" style="width:100%; margin-top: 20px;">
            <thead>
            <tr style="white-space: nowrap;">
                <th>Nome Hotel</th>
                <th>Endereço</th>
                <th>Checkin</th>
                <th>Checkout</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Nome Hotel</th>
                <th>Endereço</th>
                <th>Checkin</th>
                <th>Checkout</th>
            </tr>
            </tfoot>
            <tbody>
            <?php
            foreach ($reserves as $reserve) {
                echo "<tr style='white-space: nowrap;'>
                            <td style='width: 100px'>" . $reserve['hotel'] . "</td>" .
                    "<td style='width: 100px'>" . $reserve['address'] . "</td>" .
                    "<td style='width: 100px'>" . $reserve['checkin_date'] . "</td>" .
                    "<td style='width: 100px'>" . $reserve['checkout_date'] . "</td>" . "
                     </tr>";
            }
            ?>
            </tbody>
        </table>
    </div>
</div>


</body>
</html>