<?php
require_once 'layout/layout.php';
require_once 'Core/UtilFunctions.php';
require_once 'conexao/conectaBanco.php';

if (!isset($_SESSION['user']))
    UtilFunctions::redirect('index.php');


if (!isset($_GET['id']))
    UtilFunctions::redirect('hotelaria.php');

$hotelId = $_GET['id'];
$userId = $_SESSION['user']['id'];
?>
<!DOCTYPE html>
<!--<style>-->
<!--    a, p, td, div, tr, th {-->
<!--        color: white;-->
<!--    }-->
<!--</style>-->
<html lang="pt_BR">
<head>
    <title>Reservar</title>
</head>
<body>
<div class="container py-5">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header" style="background: #fbfbfb">
                    <strong>Reservar</strong>
                </div>
            </div>
        </div>

        <div class="col-12 card mt-4 shadow-sm" style="background: #fbfbfb">
            <div class="card-body">
                <form action="<?php echo "confirm-reserve.php?hid=$hotelId&uid=$userId" ?>" method="post"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="checkin">Checkin Date:</label>
                        <input id="checkin" type="datetime-local" name="checkin" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="checkout">Checkout Date:</label>
                        <input id="checkout" type="datetime-local" name="checkout" class="form-control" required>
                    </div>                    
                    <div>
                        <a href="hotelaria.php" class="btn btn-danger">Voltar</a>
                    <input type="submit" class="btn btn-success" style="cursor: pointer;">
                  </div>
                </form>
            </div>
        </div>

    </div>
</div>
</body>
</html>
